#ifndef PLATAFORMA_H
#define PLATAFORMA_H
#include <SFML/Graphics.hpp>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include "Jugador.h"


class Plataforma
{
    public:
        Plataforma(sf::Vector2f posicion, sf::Vector2f tamano, int tipo);
        virtual ~Plataforma();
        void draw(sf::RenderWindow &window);
        void update(Jugador &j);
        sf::FloatRect getGlobalBounds();



    private:
      sf::RectangleShape pl;
      int tipoPl;
      //Si es 0 es plataforma, si es 1 es escalera

};

#endif // PLATAFORMA_H
