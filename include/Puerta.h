#ifndef PUERTA_H
#define PUERTA_H
#include <SFML/Graphics.hpp>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include "Jugador.h"


class Puerta
{
    public:
        Puerta();
        virtual ~Puerta();
        void draw(sf::RenderWindow &window);
        void update(Jugador p1, int i);
        sf::FloatRect getGB();


    protected:

    private:
        sf::RectangleShape puerta;
        sf::Texture texture;
        sf::Sprite sprite;
};

#endif // PUERTA_H
