#ifndef JUEGO_H
#define JUEGO_H
#include <SFML/Graphics.hpp>
#include "Mapa.h"
#include "Jugador.h"
#include "Mapa.h"
#include "Objeto.h"
#include "Puerta.h"


class Juego
{
    public:
        Juego();
        virtual ~Juego();
        void iniciar(sf::RenderWindow &window);
        void update(float elapsedTime);

    private:
        sf::Time timeStartUpdate;
        //Crear relojes para el bucle tipo 3
        sf::Clock clock1;
        sf::Clock clock2;
        //Crear jugador
        Jugador p1;
        //Crear mapa
        Mapa* mapa;
        sf::View vista;
        std::vector<Objeto> esferas;
        sf::RectangleShape tele1;
        sf::RectangleShape tele2;
        Puerta p;
        int esferasRecogidas;

};

#endif // JUEGO_H
