#ifndef OBJETO_H
#define OBJETO_H
#include <SFML/Graphics.hpp>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include "Jugador.h"


class Objeto
{
    public:
        Objeto(sf::Vector2f);
        virtual ~Objeto();
        void draw(sf::RenderWindow &window);
        bool update(sf::RectangleShape hitboxBajo);

    private:
        sf::CircleShape esfera;
        bool desaparecer;
        sf::Texture textura;
        sf::Sprite sprite;
};

#endif // OBJETO_H
