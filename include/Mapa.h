#ifndef MAPA_H
#define MAPA_H
#include "tinyxml2.h"
#include "Plataforma.h"
#include <iostream>;
#include <SFML/Graphics.hpp>


using namespace std;
using namespace sf;
using namespace tinyxml2;


class Mapa
{
    public:
        Mapa(int nivel);
        ~Mapa();
        void inicializar(int nvl);
        void cargarFondo();
        void loadTextures();
        void createSprites();
        void getLayers();
        void creaColisiones();
        std::vector<Plataforma> getBloques();


        void draw(RenderWindow& target);

    private:
        int width, height;
        int tileWidth, tileHeight;
        int gid;

        XMLDocument docum;
        XMLElement* xmlMap;
        XMLElement* tileSetImg;
        XMLElement* data;
        XMLElement* layer;
        XMLElement* objectgroup;
        XMLElement* object;
        XMLElement* fondo;

        string ficheroImagen;
        Texture tileSetTexture;

        int numLayers = 0;
        int*** tileMap;

        Sprite* tilesetSprite;
        Sprite**** mapSprite;

        sf::Texture texture;
        sf::Sprite sprite;

        int nivel;

        std::vector<Plataforma> bloques;
};


#endif // MAPA_H
