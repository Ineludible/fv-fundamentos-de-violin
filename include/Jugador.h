#ifndef JUGADOR_H
#define JUGADOR_H
#include <SFML/Graphics.hpp>
#include <iostream>
#include <stdlib.h>
#include <vector>


class Jugador
{
    public:
        Jugador();
        virtual ~Jugador();
        void draw(sf::RenderWindow &window);
        void update(float tiempo);
        sf::RectangleShape getHitboxB();
        sf::RectangleShape getHitboxA();
        sf::RectangleShape getHitboxD();
        sf::RectangleShape getHitboxI();
        void colocadores();
        void moverHitboxs(sf::Vector2f);
        void setColisionaAR(bool c);
        void setColisionaA(bool c);
        void setColisionaD(bool c);
        void setColisionaI(bool c);
        void setEscalera(bool e);
        void salto(float deltaTime);
        void teletransporte(sf::Vector2f pos);
        sf::Vector2f getPosition();

    private:
        sf::CircleShape jugador;
        sf::RectangleShape hitboxBajo;
        sf::RectangleShape hitboxAlto;
        sf::RectangleShape hitboxIzq;
        sf::RectangleShape hitboxDer;
        sf::Vector2f playerCenter;
        sf::Texture texture;
        sf::Sprite sprite;
        sf::Vector2f movement;


        bool colisionaAbajo;
        bool colisionaDerecha;
        bool colisionaIzquierda;
        bool colisionaArriba;
        bool escalera;
        bool noVueles;

        //Animacion
        sf::Clock relojAnim;
        enum Direction { Down, Left, Right, Up};
        sf::Vector2f source;

        //Salto
        float jumpF;
        float gravityAcceleration;
        float mass;
        float speedV;
        bool whileJump;
};

#endif // JUGADOR_H
