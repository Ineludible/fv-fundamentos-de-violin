#include <SFML/Graphics.hpp>
#include "Juego.h"

int main()
{
    //Caracteristicas de la ventana
    sf::Vector2i screenDimensions(800,700);

    sf::RenderWindow window(sf::VideoMode(screenDimensions.x, screenDimensions.y), "Kings Valley 2");
    window.setFramerateLimit(60);

    Juego juego;
    juego.iniciar(window);

    return 0;
}
