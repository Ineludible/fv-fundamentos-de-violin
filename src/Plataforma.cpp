#include "Plataforma.h"


Plataforma::Plataforma(sf::Vector2f posicion, sf::Vector2f tamano, int tipo)
{
    //ctor
	pl = sf::RectangleShape(tamano);
	pl.setPosition(posicion);
	pl.setFillColor(sf::Color::Transparent);
	tipoPl = tipo;

	if(tipoPl==1){
        pl.setFillColor(sf::Color::Transparent);
	}

}

Plataforma::~Plataforma()
{
    //dtor
}

void Plataforma::draw(sf::RenderWindow &window){

         window.draw(pl);
}

void Plataforma::update(Jugador &jugador){


        if(tipoPl==0){
            if(jugador.getHitboxB().getGlobalBounds().intersects(pl.getGlobalBounds())){
                jugador.setColisionaA(true);
            } else if(jugador.getHitboxD().getGlobalBounds().intersects(pl.getGlobalBounds())){
                jugador.setColisionaD(true);
            } else if(jugador.getHitboxI().getGlobalBounds().intersects(pl.getGlobalBounds())){
                jugador.setColisionaI(true);
            } else if(jugador.getHitboxA().getGlobalBounds().intersects(pl.getGlobalBounds())){
               jugador.setColisionaAR(true);
            }
        } else if(tipoPl==1){

              if(jugador.getHitboxA().getGlobalBounds().intersects(pl.getGlobalBounds()) ||
                 jugador.getHitboxB().getGlobalBounds().intersects(pl.getGlobalBounds()) ||
                 jugador.getHitboxD().getGlobalBounds().intersects(pl.getGlobalBounds()) ||
                 jugador.getHitboxI().getGlobalBounds().intersects(pl.getGlobalBounds())
                 ){
                    jugador.setEscalera(true);
                 }
//            if(!jugador.getHitboxA().getGlobalBounds().intersects(pl.getGlobalBounds()) ||
//               jugador.getHitboxB().getGlobalBounds().intersects(pl.getGlobalBounds())){
//                    jugador.setEscalera(true);
//                    jugador.setColisionaA(true);
//
//
//            } else if(jugador.getHitboxA().getGlobalBounds().intersects(pl.getGlobalBounds()) &&
//               !jugador.getHitboxB().getGlobalBounds().intersects(pl.getGlobalBounds())){
//                    jugador.setColisionaA(true);
//                    jugador.setEscalera(true);
//
//               } else if(jugador.getHitboxA().getGlobalBounds().intersects(pl.getGlobalBounds()) &&
//               jugador.getHitboxB().getGlobalBounds().intersects(pl.getGlobalBounds())){
//                    jugador.setEscalera(true);
//                    jugador.setColisionaA(true);
//               }
        }
}

sf::FloatRect Plataforma::getGlobalBounds(){
    return pl.getGlobalBounds();
}
