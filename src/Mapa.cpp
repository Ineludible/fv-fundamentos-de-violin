#include "Mapa.h"

Mapa::Mapa(int n)
{
    nivel=n;
    inicializar(n);
    cargarFondo();
    loadTextures();
    createSprites();
}

Mapa::~Mapa()
{
    //dtor

}

void Mapa::inicializar(int nivel)
{
    //cargar mapa

    if(nivel==1){
        docum.LoadFile("img/Mapa.tmx");
    } else if(nivel==2){
    docum.LoadFile("img/Mapa2/Mapa2.tmx");
    }

    xmlMap = docum.FirstChildElement("map");
    //Almacenar atributos del XML
    xmlMap->QueryIntAttribute("width", &width);
    xmlMap->QueryIntAttribute("height", &height);
    xmlMap->QueryIntAttribute("tilewidth", &tileWidth);
    xmlMap->QueryIntAttribute("tileheight", &tileHeight);
    tileSetImg = xmlMap->FirstChildElement("tileset");
    layer = xmlMap->FirstChildElement("layer");
}

void Mapa::cargarFondo(){
    //Somewhere beyond the sea, somewhere waiting for me
    fondo = xmlMap->FirstChildElement("imagelayer")->FirstChildElement("image");
    //No gods or kings, only man
    while(fondo){
        string directorio = (string)fondo->Attribute("source");

        if (!texture.loadFromFile("img/"+directorio))
        std::cout<<"No se ha podido cargar el mapa"<<std::endl;
        //A man chooses, a slave obeys
        sprite.setTexture(texture);
        fondo = fondo->NextSiblingElement("image");
    }
}


void Mapa::loadTextures()
{
    while(tileSetImg) {
        if(!tileSetTexture.loadFromFile("img/Mapa2/spritesheet-mapa.png")){
            std::cerr << "No se ha podido cargar la textura" << std::endl;
            exit(0);
        }
        tileSetImg = tileSetImg->NextSiblingElement("tileset");
    }

    getLayers();

    layer = xmlMap->FirstChildElement("layer");
    creaColisiones();
    tileMap = new int**[numLayers];

    for(int i = 0; i < numLayers; i++) {
        tileMap[i] = new int*[height];
        for(int j = 0; j < height; j++) {
            tileMap[i][j] = new int[width];
        }
    }

    int l = 0;
    while(layer) {
        data = layer->FirstChildElement("data")->FirstChildElement("tile");

        while(data) {
            for(int i = 0; i < height; i++) {
                for(int j = 0; j < width; j++) {
                    data->QueryIntAttribute("gid", &tileMap[l][i][j]);
                    data = data->NextSiblingElement("tile");
                }
            }
        }
        layer = layer->NextSiblingElement("layer");
        l++;
    }
}


void Mapa::getLayers()
{
    while(layer){
        numLayers++;
        layer = layer->NextSiblingElement("layer");
    }
}


void Mapa::createSprites()
{
    mapSprite = new Sprite***[numLayers];

    for(int i = 0; i < numLayers; i++) {
        mapSprite[i] = new Sprite**[height];
        for(int j = 0; j < height; j++) {
            mapSprite[i][j] = new Sprite*[width];
            for(int z = 0; z < width; z++) {
                mapSprite[i][j][z] = new Sprite;
            }
        }
    }

    int cols = tileSetTexture.getSize().x / tileWidth;
    int rows = tileSetTexture.getSize().y / tileHeight;

    tilesetSprite = new Sprite[rows*cols];

    int n = 0;
    for(int i = 0; i < rows; i++) {
        for(int j = 0; j < cols; j++) {
            tilesetSprite[n].setTexture(tileSetTexture);
            tilesetSprite[n].setTextureRect(IntRect(j*tileWidth, i*tileHeight, tileWidth, tileHeight));
            n++;
        }
    }

    for(int i = 0; i < numLayers; i++) {
        for(int j = 0; j < height; j++) {
            for(int k = 0; k < width; k++) {
                gid = tileMap[i][j][k]-1;

                if(gid > 0 && gid < width*height) {
                    mapSprite[i][j][k] = new Sprite(tileSetTexture, tilesetSprite[gid].getTextureRect());
                    mapSprite[i][j][k]->setPosition(k*tileWidth, j*tileHeight);
                } else {
                    mapSprite[i][j][k] = NULL;
                }

            }
        }
    }

}

void Mapa::creaColisiones(){
   objectgroup = xmlMap->FirstChildElement("objectgroup");

   while(objectgroup) {

     object = objectgroup->FirstChildElement("object");
     while(object) {
        string nom = (string)objectgroup->Attribute("name");
        if(nom.compare("escalera") == 0){

        sf::Vector2f pos(atoi(object->Attribute("x")), atoi(object->Attribute("y")));
        sf::Vector2f tam(atoi(object->Attribute("width")), atoi(object->Attribute("height")));

        Plataforma bloque(pos, tam, 1);
        bloques.push_back(bloque);
        object = object->NextSiblingElement("object");

        } else{
            sf::Vector2f pos(atoi(object->Attribute("x")), atoi(object->Attribute("y")));
            sf::Vector2f tam(atoi(object->Attribute("width")), atoi(object->Attribute("height")));

            Plataforma bloque(pos, tam, 0);
            bloques.push_back(bloque);
            object = object->NextSiblingElement("object");
            }
     }
     objectgroup = objectgroup->NextSiblingElement("objectgroup");
    }
}


void Mapa::draw(RenderWindow& target)
{
    target.draw(sprite);
    for(int l = 0; l < numLayers; l++) {
        for(int y = 0; y < height; y++) {
            for(int x = 0; x < width; x++) {
                if(mapSprite[l][y][x] !=NULL) {
                    target.draw(*(mapSprite[l][y][x]));
                }
            }
        }
    }
    for(int i = 0; i<bloques.size(); i++){
        bloques[i].draw(target);
    }
}


std::vector<Plataforma> Mapa::getBloques(){
    return bloques;
}


