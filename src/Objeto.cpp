#include "Objeto.h"

Objeto::Objeto(sf::Vector2f posicion)
{
    //ctor
    if(!textura.loadFromFile("img/esfera.png")){
        std::cout<<"No se cargo el sprite de la esfera"<<std::endl;
    }
    sprite.setTexture(textura);
    sprite.setPosition(posicion);
    esfera = sf::CircleShape(30);
    esfera.setPosition(posicion);
    esfera.setFillColor(sf::Color::Transparent);
    desaparecer = false;
}

Objeto::~Objeto()
{
    //dtor
}

void Objeto::draw(sf::RenderWindow &window){

    if(desaparecer==false){

        window.draw(esfera);
         window.draw(sprite);
    }
}

bool Objeto::update(sf::RectangleShape hitboxBajo){
  if(hitboxBajo.getGlobalBounds().intersects(esfera.getGlobalBounds())){
        desaparecer = true;
    }
    return desaparecer;
}

