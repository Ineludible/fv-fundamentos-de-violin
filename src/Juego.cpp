#include "Juego.h"
#define kUpdateTimePS 1000/30

Juego::Juego()
{
    //ctor
    esferasRecogidas = 0;

}

Juego::~Juego()
{
    //dtor
}

void Juego::update(float elapsedTime){
        //update
        //movimiento jugador
        p1.update(elapsedTime);

        //colision con bloques
        for(int i=0; i<mapa->getBloques().size(); i++){
            mapa->getBloques().at(i).update(p1);
        }

        //colision con esferas
        for(int i=0; i<esferas.size(); i++){
            if(esferas[i].update(p1.getHitboxI()) || esferas[i].update(p1.getHitboxD())){
                std::cout<< "Se elimina la esfera"<<std::endl;
                esferas.erase(esferas.begin()+i);
                esferasRecogidas++;
            }
        }

        if(p1.getHitboxA().getGlobalBounds().intersects(tele2.getGlobalBounds())){
            p1.teletransporte(sf::Vector2f(p1.getPosition().x,0));
        }

        if(p1.getHitboxB().getGlobalBounds().intersects(tele1.getGlobalBounds())){
            p1.teletransporte(sf::Vector2f(p1.getPosition().x,750));
        }
        p.update(p1, esferasRecogidas);

        if(esferasRecogidas==4 && p.getGB().intersects(p1.getHitboxA().getGlobalBounds())){
            mapa = new Mapa(2);
        }

}

void Juego::iniciar(sf::RenderWindow &app){

    //Crear mapa
    mapa = new Mapa(1);

    //Crear vista para alejar un poco el mapa
    vista.zoom(1.05f);
    vista.setCenter(515.f,420.f);


    //Crear vector de esferas
    Objeto esfera(sf::Vector2f(288,385));
    Objeto esfera1(sf::Vector2f(155,255));
    Objeto esfera2(sf::Vector2f(670,385));
    Objeto esfera3(sf::Vector2f(800,255));
    esferas.push_back(esfera);
    esferas.push_back(esfera1);
    esferas.push_back(esfera2);
    esferas.push_back(esfera3);

    //teletransporte
    tele1.setSize(sf::Vector2f(80,20));
    tele1.setPosition(sf::Vector2f(790,0));
    tele2.setSize(sf::Vector2f(80,20));
    tele2.setPosition(sf::Vector2f(790,760));



    timeStartUpdate = clock1.getElapsedTime();

	// Start the game loop
    while (app.isOpen())
    {
        // Process events
        sf::Event event;
        while (app.pollEvent(event))
        {
            // Close window : exit
            if (event.type == sf::Event::Closed)
                app.close();
        }

        if(clock1.getElapsedTime().asMilliseconds() - timeStartUpdate.asMilliseconds() > kUpdateTimePS){
            update(clock2.restart().asSeconds());
            timeStartUpdate = clock1.getElapsedTime();
        }
        // Clear screen
        app.clear();
        app.setView(vista);

        //draw

        mapa->draw(app);
        p.draw(app);
        p1.draw(app);
        for(int i=0; i<esferas.size(); i++){
            esferas[i].draw(app);
        }

        // Update the window
        app.display();
    }
}
