#include "Puerta.h"

Puerta::Puerta()
{
    //ctor
    puerta.setSize(sf::Vector2f(128,128));
    puerta.setPosition(sf::Vector2f(400,62));
    sprite.setPosition(puerta.getPosition());
}

Puerta::~Puerta()
{
    //dtor
}

void Puerta::draw(sf::RenderWindow &window){
    window.draw(puerta);
    window.draw(sprite);
}

void Puerta::update(Jugador p1, int i){
    if(i==0){
        if (!texture.loadFromFile("img/Puertas/1.png"))
            std::cout<<"No se cargó el spritesheet"<<std::endl;
    } else if(i==1){
        if (!texture.loadFromFile("img/Puertas/2.png"))
            std::cout<<"No se cargó el spritesheet"<<std::endl;
    } else if(i==2){
        if (!texture.loadFromFile("img/Puertas/3.png"))
            std::cout<<"No se cargó el spritesheet"<<std::endl;
    } else if(i==3){
        if (!texture.loadFromFile("img/Puertas/4.png"))
            std::cout<<"No se cargó el spritesheet"<<std::endl;
    } else if(i==4){
        if (!texture.loadFromFile("img/Puertas/5.png"))
            std::cout<<"No se cargó el spritesheet"<<std::endl;
    }
    sprite.setTexture(texture);
}

sf::FloatRect Puerta::getGB(){
    return puerta.getGlobalBounds();
}
