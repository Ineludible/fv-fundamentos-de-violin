#include "Jugador.h"

Jugador::Jugador()
{
    //ctor
    jugador = sf::CircleShape(25);
    jugador.setPosition(sf::Vector2f(80,630));
    sf::Vector2f playerCenter;
    hitboxBajo = sf::RectangleShape(sf::Vector2f(40.f,10.f));
	hitboxBajo.setFillColor(sf::Color::Transparent);

	hitboxAlto = sf::RectangleShape(sf::Vector2f(40.f,10.f));
	hitboxAlto.setFillColor(sf::Color::Transparent);

	hitboxIzq = sf::RectangleShape(sf::Vector2f(10.f,30.f));
	hitboxIzq.setFillColor(sf::Color::Transparent);

	hitboxDer = sf::RectangleShape(sf::Vector2f(10.f,30.f));
	hitboxDer.setFillColor(sf::Color::Transparent);
	colisionaAbajo=false;
	colisionaDerecha=false;
	colisionaIzquierda=false;
	colisionaArriba=false;
	escalera=false;

	colocadores();

	 if (!texture.loadFromFile("img/spritesheet.png"))
        std::cout<<"No se cargó el spritesheet"<<std::endl;
    sprite.setTexture(texture);

    source = sf::Vector2f(1, Down);

    //Salto
    jumpF = 170;
    gravityAcceleration=9.8;
    mass=0;
    speedV=0;
    noVueles=false;
    whileJump = false;
}

Jugador::~Jugador()
{
    //dtor
}

sf::RectangleShape Jugador::getHitboxB(){
    return hitboxBajo;
}

sf::RectangleShape Jugador::getHitboxA(){
    return hitboxAlto;
}

sf::RectangleShape Jugador::getHitboxD(){
    return hitboxDer;
}

sf::RectangleShape Jugador::getHitboxI(){
    return hitboxIzq;
}

void Jugador::draw(sf::RenderWindow &window){
   window.draw(jugador);
   window.draw(hitboxBajo);
   window.draw(hitboxAlto);
   window.draw(hitboxIzq);
   window.draw(hitboxDer);
   window.draw(sprite);

}

void Jugador::salto(float deltaTime){
    if(!colisionaArriba){
    speedV-=gravityAcceleration*deltaTime;
    jugador.move(0, -speedV);
    sf::Vector2f mov(0, -speedV);
    moverHitboxs(mov);
    }
}

void Jugador::colocadores() {

    playerCenter = sf::Vector2f(jugador.getPosition().x, jugador.getPosition().y+jugador.getRadius());
    hitboxBajo.setPosition(playerCenter.x + 7, playerCenter.y + 25);
    hitboxAlto.setPosition(playerCenter.x + 7, playerCenter.y - 25);
    hitboxIzq.setPosition(playerCenter.x - 2, playerCenter.y - 12);
    hitboxDer.setPosition(playerCenter.x + 50, playerCenter.y - 10);
    sprite.setPosition(playerCenter.x, playerCenter.y-30);

}

void Jugador::moverHitboxs(sf::Vector2f movimiento) {
    hitboxAlto.move(movimiento);
    hitboxBajo.move(movimiento);
    hitboxDer.move(movimiento);
    hitboxIzq.move(movimiento);
    sprite.move(movimiento);
}


void Jugador::update(float tiempo){

    bool andar=false;
    float speed = 200.f;
    mass=10;


         //Movimiento
        movement = sf::Vector2f(0.f, 0.f);

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && colisionaIzquierda == false)
        {
            andar=true;
            source.y = Left;
            movement.x -= speed;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && colisionaDerecha == false)
        {
            andar=true;
            source.y = Right;
            movement.x += speed;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && escalera==true && colisionaArriba == false)
        {
            andar=true;
            source.y = Up;
            movement.y -= speed;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && escalera==true && colisionaAbajo == false)
        {
            andar=true;
            source.y = Down;
            movement.y += speed;
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && noVueles==false){
            noVueles = true;
            speedV=jumpF/mass;
            whileJump = true;

        }


        if(whileJump){
            salto(tiempo);
            if(colisionaAbajo==true){
                whileJump=false;

            }
        }

        if(colisionaAbajo){
            noVueles=false;
        }

        if(colisionaAbajo==false && escalera == false){
            //movement.y += gravityAcceleration*tiempo;
            movement.y += speed;

        }

        jugador.move(movement * tiempo);
        moverHitboxs(movement * tiempo);

        colisionaAbajo=false;
        colisionaDerecha=false;
        colisionaIzquierda=false;
        colisionaArriba=false;
        escalera=false;

        if(relojAnim.getElapsedTime().asSeconds() >= 0.1f){
            if(andar==true){
                source.x++;
            if(source.x * 64 >= texture.getSize().x)
                source.x=0;
            }

            relojAnim.restart();
        }

        sprite.setTextureRect(sf::IntRect(source.x * 64, source.y * 64, 64, 64));
}

void Jugador::setColisionaA(bool b){
    colisionaAbajo=b;
}

void Jugador::setColisionaAR(bool b){
    colisionaArriba=b;
}

void Jugador::setColisionaI(bool b){
    colisionaIzquierda=b;
}

void Jugador::setColisionaD(bool b){
    colisionaDerecha=b;
}

void Jugador::setEscalera(bool e){
    escalera=e;
}

sf::Vector2f Jugador::getPosition(){
    return jugador.getPosition();
}

void Jugador::teletransporte(sf::Vector2f pos){
    jugador.setPosition(pos);
    //sprite.setPosition(pos.x, pos.y);
    colocadores();
}




